# Settings For Garuda Linux Cinnamon Edition

## Gestures

1. Swipe 3 fingers left/right for back/forward respectively.
2. Swipe 4 fingers left/right for changing workspaces.
3. Swipe 3/4 fingers down for showing desktop.
4. Swipe 3/4 fingers up for showing windows selection screen.